const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const sigUtil = require('eth-sig-util')
// Run `npm install cors`, then uncomment below.
/* Swap asterisk (*) to slash (/) to uncomment.
const cors = require('cors');
// */
const PORT = process.env.PORT || 5000
const app = express()

function handleError(res, reason, code) {
  console.log("ERROR: " + reason);
  res.status(code || 400).json({"error": reason});
}

/* Swap asterisk (*) to slash (/) to uncomment.
app.use(cors({
    origin: 'http://alloweddomain.com'
}));
// */
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// Don't uncomment these 2.
// POST to /ethpress with { signature, message }, both regular strings.
app.post('/ethpress', (req, res) => {
    if ( ! req.body.signature || ! req.body.message ) {
        handleError(res, 'Invalid arguments. Need {signature, message}')
        return
    }
    try {
        const sig = sigUtil.normalize(req.body.signature)
        const msg = sigUtil.normalize(Buffer.from(req.body.message, 'utf8').toString('hex'))
        const address = sigUtil.recoverPersonalSignature({
            data: msg,
            sig: sig
        })
        res.json({ data: address})
    } catch (e) {
        handleError(res, e.message)
    }
  })
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))
