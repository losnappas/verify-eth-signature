# EthPress: Verify Ethereum Signature

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) installed.

```sh
$ git clone https://gitlab.com/losnappas/verify-eth-signature # or download it from the page
$ cd verify-eth-signature
$ npm install
$ npm start
```

Your verification service should now be running on `http://localhost:5000/ethpress`. On the EthPress options page in WordPress, edit the verification service to match that.

Useful thread: https://wordpress.org/support/topic/verify-eth-signature/
